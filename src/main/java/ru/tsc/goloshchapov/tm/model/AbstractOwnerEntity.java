package ru.tsc.goloshchapov.tm.model;

public abstract class AbstractOwnerEntity extends AbstractEntity {

    protected String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
