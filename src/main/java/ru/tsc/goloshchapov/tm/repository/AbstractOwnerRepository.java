package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.IOwnerRepository;
import ru.tsc.goloshchapov.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public void add(final String userId, E entity) {
        entity.setUserId(userId);
        add(entity);
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return;
        remove(entity);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .anyMatch(e -> id.equals(e.getId()));
    }

    @Override
    public boolean existsByIndex(final String userId, final Integer index) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null) != null;
    }

    @Override
    public List<E> findAll(final String userId) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findById(final String userId, final String id) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public void clear(final String userId) {
        final List<E> entitiesForRemoveWithUserId = findAll(userId);
        entities.removeAll(entitiesForRemoveWithUserId);
    }

}
