package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.repository.ICommandRepository;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;

import java.util.*;
import java.util.stream.Collectors;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getCommandNames() {
        return commands.values()
                .stream()
                .filter(c -> c.name() != null)
                .map(e -> e.name())
                .collect(Collectors.toList());
    }

    @Override
    public Collection<String> getCommandArg() {
        return commands.values()
                .stream()
                .filter(a -> a.arg() != null)
                .map(e -> e.arg())
                .collect(Collectors.toList());
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

}
