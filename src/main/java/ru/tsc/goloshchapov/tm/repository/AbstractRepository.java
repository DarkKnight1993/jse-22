package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.IRepository;
import ru.tsc.goloshchapov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public void add(E entity) {
        entities.add(entity);
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

    @Override
    public boolean existsById(final String id) {
        return entities.stream().anyMatch(e -> id.equals(e.getId()));
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        return entities.stream()
                .skip(index)
                .findFirst()
                .orElse(null) != null;
    }

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        return entities.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findById(final String id) {
        return entities.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public E findByIndex(final Integer index) {
        return entities.stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public E removeById(final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public E removeByIndex(final Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public void clear() {
        entities.clear();
    }

}
