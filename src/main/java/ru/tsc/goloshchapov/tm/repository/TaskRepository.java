package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.repository.ITaskRepository;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.model.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public Task findByName(final String userId, final String name) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .filter(e -> name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(this::remove);
        return task.orElse(null);
    }

    @Override
    public Task startById(final String userId, final String id) {
        final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(t -> {
            t.setStatus(Status.IN_PROGRESS);
            t.setStartDate(new Date());
        });
        return task.orElse(null);
    }

    @Override
    public Task startByIndex(final String userId, final Integer index) {
        final Optional<Task> task = Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(t -> {
            t.setStatus(Status.IN_PROGRESS);
            t.setStartDate(new Date());
        });
        return task.orElse(null);
    }

    @Override
    public Task startByName(final String userId, final String name) {
        final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(t -> {
            t.setStatus(Status.IN_PROGRESS);
            t.setStartDate(new Date());
        });
        return task.orElse(null);
    }

    @Override
    public Task finishById(final String userId, final String id) {
        final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(t -> {
            t.setStatus(Status.COMPLETED);
            t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @Override
    public Task finishByIndex(final String userId, final Integer index) {
        final Optional<Task> task = Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(t -> {
            t.setStatus(Status.COMPLETED);
            t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(t -> {
            t.setStatus(Status.COMPLETED);
            t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(t -> {
            t.setStatus(status);
            if (status == Status.IN_PROGRESS) t.setStartDate(new Date());
            if (status == Status.COMPLETED) t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) {
        final Optional<Task> task = Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(t -> {
            t.setStatus(status);
            if (status == Status.IN_PROGRESS) t.setStartDate(new Date());
            if (status == Status.COMPLETED) t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @Override
    public Task changeStatusByName(final String userId, final String name, final Status status) {
        final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(t -> {
            t.setStatus(status);
            if (status == Status.IN_PROGRESS) t.setStartDate(new Date());
            if (status == Status.COMPLETED) t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, final String projectId) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .filter(t -> projectId.equals(t.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, final String projectId) {
        Optional<List<Task>> listByProject = Optional.ofNullable(findAllTaskByProjectId(userId, projectId));
        if (listByProject.isPresent()) entities.removeAll(listByProject.get());
    }

    @Override
    public Task bindTaskToProjectById(final String userId, final String projectId, final String taskId) {
        final Optional<Task> task = Optional.ofNullable(findById(userId, taskId));
        task.ifPresent(t -> t.setProjectId(projectId));
        return task.orElse(null);
    }

    @Override
    public Task unbindTaskById(final String userId, final String taskId) {
        final Optional<Task> task = Optional.ofNullable(findById(userId, taskId));
        task.ifPresent(t -> t.setProjectId(null));
        return task.orElse(null);
    }

}
