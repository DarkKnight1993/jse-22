package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.repository.IProjectRepository;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.model.Project;

import java.util.Date;
import java.util.Optional;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public Project findByName(final String userId, final String name) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .filter(p -> name.equals(p.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(this::remove);
        return project.orElse(null);
    }

    @Override
    public Project startById(final String userId, final String id) {
        final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> {
            p.setStatus(Status.IN_PROGRESS);
            p.setStartDate(new Date());
        });
        return project.orElse(null);
    }

    @Override
    public Project startByIndex(final String userId, final Integer index) {
        final Optional<Project> project = Optional.ofNullable(findByIndex(userId, index));
        project.ifPresent(p -> {
            p.setStatus(Status.IN_PROGRESS);
            p.setStartDate(new Date());
        });
        return project.orElse(null);
    }

    @Override
    public Project startByName(final String userId, final String name) {
        final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(p -> {
            p.setStatus(Status.IN_PROGRESS);
            p.setStartDate(new Date());
        });
        return project.orElse(null);
    }

    @Override
    public Project finishById(final String userId, final String id) {
        final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> {
            p.setStatus(Status.COMPLETED);
            p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

    @Override
    public Project finishByIndex(final String userId, final Integer index) {
        final Optional<Project> project = Optional.ofNullable(findByIndex(userId, index));
        project.ifPresent(p -> {
            p.setStatus(Status.COMPLETED);
            p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

    @Override
    public Project finishByName(final String userId, final String name) {
        final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(p -> {
            p.setStatus(Status.COMPLETED);
            p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

    @Override
    public Project changeStatusById(final String userId, final String id, final Status status) {
        final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> {
            p.setStatus(status);
            if (status == Status.IN_PROGRESS) p.setStartDate(new Date());
            if (status == Status.COMPLETED) p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

    @Override
    public Project changeStatusByIndex(final String userId, final Integer index, final Status status) {
        final Optional<Project> project = Optional.ofNullable(findByIndex(userId, index));
        project.ifPresent(p -> {
            p.setStatus(status);
            if (status == Status.IN_PROGRESS) p.setStartDate(new Date());
            if (status == Status.COMPLETED) p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

    @Override
    public Project changeStatusByName(final String userId, final String name, final Status status) {
        final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(p -> {
            p.setStatus(status);
            if (status == Status.IN_PROGRESS) p.setStartDate(new Date());
            if (status == Status.COMPLETED) p.setFinishDate(new Date());
        });
        return project.orElse(null);
    }

}
