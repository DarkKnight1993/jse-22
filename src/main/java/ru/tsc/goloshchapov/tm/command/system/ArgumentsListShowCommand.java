package ru.tsc.goloshchapov.tm.command.system;

import ru.tsc.goloshchapov.tm.command.AbstractCommand;
import ru.tsc.goloshchapov.tm.command.AbstractSystemCommand;

import java.util.Collection;

public final class ArgumentsListShowCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String description() {
        return "Display list of arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand command : commands) showCommandValue(command.arg());
    }

}
