package ru.tsc.goloshchapov.tm.command.auth;

import ru.tsc.goloshchapov.tm.command.AbstractAuthCommand;

public final class AuthLogoutCommand extends AbstractAuthCommand {
    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout user from app";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
    }
}
