package ru.tsc.goloshchapov.tm.command.user;

import ru.tsc.goloshchapov.tm.command.AbstractUserCommand;
import ru.tsc.goloshchapov.tm.enumerated.Role;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public class UserByLoginLockCommand extends AbstractUserCommand {
    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
