package ru.tsc.goloshchapov.tm.api;

import ru.tsc.goloshchapov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    void add(String userId, E entity);

    void remove(String userId, E entity);

    boolean existsById(String userId, String id);

    boolean existsByIndex(String userId, Integer index);

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findById(String userId, String id);

    E findByIndex(String userId, Integer index);

    E removeById(String userId, String id);

    E removeByIndex(String userId, Integer index);

    void clear(String userId);
}
